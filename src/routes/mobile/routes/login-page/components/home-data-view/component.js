module.exports = class {

    onCreate() {
        this.state = {
            record: null,
            table_data: null,
            hidden: true,
            selected_total: 0
        }
    }

    update_records(records) {
        let table_data = records.reduce((acc, f) => {
            acc.patients[f[9]] = (acc.patients[f[9]] ? acc.patients[f[9]] : 0) + 1;
            acc.city = f[6]
            acc.district = f[7];
            acc.state = f[8];
            return acc;
        }, {
            patients: {
                Hospitalized: 0,
                Recovered: 0,
                Deceased: 0

            }
        })
        table_data.color_codes = {
            Deceased: 'text-color-red',
            Recovered: 'text-color-green',
            Hospitalized: 'text-color-orange',
            Migrated: 'text-color-blue'
        }
        table_data.patient_keys = Object.keys(table_data.patients);
        table_data.medical_load = 60;
        table_data.civil_load = 80;
        this.state.table_data = table_data;
        this.refresh_bars(table_data.medical_load, table_data.civil_load);
    }
    refresh() {
        let record = [...window.app.data.record];;
        this.state.record = record;



    }

    refresh_bars(medical_load, civil_load) {
        setTimeout(() => {
            window.app.progressbar.set(this.getEl('medical'), medical_load, 2000)

            window.app.progressbar.set(this.getEl('civil'), civil_load, 2000)
        }, 10);
    }

}