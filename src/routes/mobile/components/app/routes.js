let routes = [
  {
    name: 'kanban',
    path: '/kanban',
    pageName: 'kanban',
  },
  {
    name: 'home-page',
    path: '/home-page',
    pageName: 'home-page',
  },
  {
    name: 'login',
    path: '/login',
    pageName: 'login',
  }
];

exports.routes = routes;