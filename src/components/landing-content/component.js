module.exports = class {

    onMount() {
        let dots = '.';
        setInterval(() => {
            this.getEl('loading').innerHTML = `Loading${dots}`
            dots += '.';
        }, 10);

        window.location = '/mobile/'
    }
}
